using Cats.Services.Models;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Cats.Extensions;
using Cats.Views.Models;
using Shouldly;
using TestStack.BDDfy;

namespace Cats.Test
{    
    [TestFixture]
    public class GroupCatsByOwnerGenderScenario
    {
        private List<Owner> _owners;
        private List<PetsGroupViewModel> _viewModels;

        [Test]
        public void FindCats()
        {
            this.Given(x => x.GivenListOfOwners())
                .When(x => x.WhenEveryOneHasPets())
                .When(x => x.WhenGroupCatByOwnerGender())
                .Then(x => x.ThenTheViewModelsShouldHave5Cats())
                .BDDfy();
        }

        [Test]
        public void FindDogs()
        {
            this.Given(x => x.GivenListOfOwners())
                .When(x => x.WhenEveryOneHasPets())
                .When(x => x.WhenGroupDogByOwnerGender())
                .Then(x => x.ThenTheViewModelsShouldHave3Dogs())
                .BDDfy();
        }

        [Test]
        public void FindFish()
        {
            this.Given(x => x.GivenListOfOwners())
                .When(x => x.WhenEveryOneHasPets())
                .When(x => x.WhenGroupFishByOwnerGender())
                .Then(x => x.ThenTheViewModelsShouldBeEmpty())
                .BDDfy();
        }

        [Test]
        public void NoneHaveAPet()
        {
            this.Given(x => x.GivenListOfOwners())
                .When(x => x.WhenGroupCatByOwnerGender())
                .Then(x => x.ThenTheViewModelsShouldBeEmpty())
                .BDDfy();
        }

        private void GivenListOfOwners()
        {
            _owners = new List<Owner>
            {
                new Owner
                {
                    Name = "Mike",
                    Age = 20,
                    Gender = Owner.GenderType.Male
                },

                new Owner
                {
                    Name = "Tim",
                    Age = 60,
                    Gender = Owner.GenderType.Male
                },

                new Owner
                {
                    Name = "Alice",
                    Age = 30,
                    Gender = Owner.GenderType.Female
                },

                new Owner()
                {
                    Name = "Peter",
                    Age = 32,
                    Gender = Owner.GenderType.Male
                }
            };
        }

        private void WhenEveryOneHasPets()
        {
            _owners[0].Pets = new List<Pet>
            {
                new Pet
                {
                    Name = "Bella",
                    Type = Pet.PetType.Cat
                },
                new Pet
                {
                    Name = "Oliver",
                    Type = Pet.PetType.Cat
                }
            };

            _owners[1].Pets = new List<Pet>
            {
                new Pet
                {
                    Name = "Max",
                    Type = Pet.PetType.Dog
                },
                new Pet
                {
                    Name = "Maggie",
                    Type = Pet.PetType.Cat
                }
            };

            _owners[2].Pets = new List<Pet>
            {
                new Pet
                {
                    Name = "Charlie",
                    Type = Pet.PetType.Cat
                }
            };

            _owners[3].Pets = new List<Pet>
            {
                new Pet
                {
                    Name = "Molly",
                    Type = Pet.PetType.Dog
                },
                new Pet
                {
                    Name = "Sophie",
                    Type = Pet.PetType.Cat
                },

                new Pet
                {
                    Name = "Chloe",
                    Type = Pet.PetType.Dog
                }
            };
        }

        private void WhenGroupCatByOwnerGender()
        {
            _viewModels = _owners.GroupPetsByOwnerGender(Pet.PetType.Cat).ToList();
        }

        private void WhenGroupDogByOwnerGender()
        {
            _viewModels = _owners.GroupPetsByOwnerGender(Pet.PetType.Dog).ToList();
        }

        private void WhenGroupFishByOwnerGender()
        {
            _viewModels = _owners.GroupPetsByOwnerGender(Pet.PetType.Fish).ToList();
        }

        private void ThenTheViewModelsShouldHave5Cats()
        {
            _viewModels.Count().ShouldBe(2);
            var maleGroup = _viewModels.First(x => x.Gender == Owner.GenderType.Male.ToString()).PetNames;
            var femaleGroup = _viewModels.First(x => x.Gender == Owner.GenderType.Female.ToString()).PetNames;

            maleGroup.Count().ShouldBe(4);
            femaleGroup.Count().ShouldBe(1);
        }

        private void ThenTheViewModelsShouldHave3Dogs()
        {
            _viewModels.Count().ShouldBe(1);
            var maleGroup = _viewModels.First(x => x.Gender == Owner.GenderType.Male.ToString()).PetNames;
            maleGroup.Count().ShouldBe(3);
        }

        private void ThenTheViewModelsShouldBeEmpty()
        {
            _viewModels.Count().ShouldBe(0);
        }
    }
}
