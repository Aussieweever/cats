﻿using System.Collections.Generic;
using Cats.Controllers;
using Cats.Services;
using Cats.Services.Models;
using Cats.Views.Models;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using Shouldly;
using TestStack.BDDfy;

namespace Cats.Test
{
    [TestFixture]
    public class MvcControllerScenario
    {
        private IPetService _petService;
        private ViewResult _viewResult;

        [Test]
        public void NoramlRendering()
        {
            this.Given(x => x.GivenThePetService())
                .When(x => x.WhenResponseReturnsData())
                .When(x => x.WhenShowHomePage())
                .Then(x => x.ThenTheViewIsIndex())
                .BDDfy();
        }
        
        private void GivenThePetService()
        {
            _petService = Substitute.For<IPetService>();
        }

        private void WhenResponseReturnsData()
        {
            #region dummy data
            var response = new Owner[]
            {
                new Owner
                {
                    Name = "Mike",
                    Age = 20,
                    Gender = Owner.GenderType.Male,
                    Pets = new List<Pet>
                    {
                        new Pet
                        {
                            Name="Hello",
                            Type = Pet.PetType.Cat
                        },
                        new Pet
                        {
                            Name = "White",
                            Type = Pet.PetType.Cat
                        }
                    }
                },

                new Owner
                {
                    Name = "Tim",
                    Age = 60,
                    Gender = Owner.GenderType.Male,
                    Pets = new List<Pet>
                    {
                        new Pet
                        {
                            Name = "Seagull",
                            Type = Pet.PetType.Dog
                        }
                    }
                },

                new Owner
                {
                    Name = "Alice",
                    Age = 30,
                    Gender = Owner.GenderType.Female,
                    Pets = new List<Pet>
                    {
                        new Pet
                        {
                            Name = "Meatball",
                            Type = Pet.PetType.Cat
                        }
                    }
                },

                new Owner()
                {
                    Name = "Peter",
                    Age = 32,
                    Gender = Owner.GenderType.Male,
                    Pets = new List<Pet>
                    {
                        new Pet
                        {
                            Name = "Nimo",
                            Type = Pet.PetType.Fish
                        }
                    }
                }
            };
            #endregion

            _petService.GetOwnersAndPets().Returns(response);
        }
        
        private void WhenShowHomePage()
        {
            var controller = new HomeController(_petService);
            var result = controller.Index();
            result.Wait();

            _viewResult = result.Result as ViewResult;
        }
        
        private void ThenTheViewIsIndex()
        {
            _viewResult.ViewName.ShouldBe("Index");
            _viewResult.Model.ShouldBeOfType<PetsGroupViewModel[]>();
            var viewModels = _viewResult.Model as PetsGroupViewModel[];
            viewModels.Length.ShouldBe(2);
        }
    }
}
