﻿using System.Collections.Generic;
using System.Linq;
using Cats.Services.Models;
using Cats.Views.Models;

namespace Cats.Extensions
{
    public static class OwnerExtension
    {
        public static PetsGroupViewModel[] GroupPetsByOwnerGender(this IEnumerable<Owner> list, Pet.PetType petType)
        {
            return list.Where(x => x.Pets.Any(y => y.Type == petType))
                .Select(x => new
                {
                    x.Gender,
                    Pets = x.Pets.Where(y => y.Type == petType).Select(y => y.Name)
                })
                .GroupBy(x => x.Gender)
                .Select(g => new PetsGroupViewModel
                {
                    Gender = g.Key.ToString(),
                    PetNames = g.SelectMany(z => z.Pets).OrderBy(z => z).ToList()
                }).ToArray();
        }
    }
}
