﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Cats.Services.Models;
using Newtonsoft.Json;

namespace Cats.Services
{
    public interface IPetService
    {
        Task<Owner[]> GetOwnersAndPets();
    }

    public class PetService : IPetService
    {
        private readonly Lazy<HttpClient> _httpClient;

        public PetService(string serviceUrl)
        {
            _httpClient = new Lazy<HttpClient>(() => new HttpClient
            {
                BaseAddress = new Uri(serviceUrl)
            });
        }

        public async Task<Owner[]> GetOwnersAndPets()
        {
            var response = await _httpClient.Value.GetAsync("");
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Owner[]>(content, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}
