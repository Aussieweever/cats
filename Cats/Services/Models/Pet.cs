﻿namespace Cats.Services.Models
{
    public class Pet
    {
        public enum PetType
        {
            Cat = 0,
            Dog,
            Fish
        }

        public string Name { get; set; }
        public PetType Type { get; set; }
    }
}
