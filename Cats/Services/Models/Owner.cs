﻿using System.Collections.Generic;

namespace Cats.Services.Models
{
    public class Owner
    {
        public enum GenderType
        {
            Male=0,
            Female
        }

        public string Name { get; set; }
        public GenderType Gender { get; set; }
        public int Age { get; set; }
        
        public List<Pet> Pets { get; set; }

        public Owner()
        {
            Pets = new List<Pet>();
        }
    }
}
