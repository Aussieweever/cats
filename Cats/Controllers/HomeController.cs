﻿using System.Threading.Tasks;
using Cats.Extensions;
using Cats.Services;
using Cats.Services.Models;
using Microsoft.AspNetCore.Mvc;

namespace Cats.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPetService _petService;

        public HomeController(IPetService petService)
        {
            _petService = petService;
        }

        public async Task<IActionResult> Index()
        {
            var owners = await _petService.GetOwnersAndPets();
            return View("Index", owners.GroupPetsByOwnerGender(Pet.PetType.Cat));
        }

        
    }
}
