﻿using System.Collections.Generic;

namespace Cats.Views.Models
{
    public class PetsGroupViewModel
    {
        public string Gender { get; set; }
        public List<string> PetNames { get; set; }

        public PetsGroupViewModel()
        {
            PetNames = new List<string>();
        }
    }
}
