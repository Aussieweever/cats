﻿using System.IO;
using Autofac;
using Cats.Services;
using Microsoft.Extensions.Configuration;

namespace Cats
{
    public class AppModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //TODO: to find a better way to get the configuration
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            builder.RegisterType<PetService>().As<IPetService>()
                .WithParameter("serviceUrl", configuration.GetValue<string>("PetServiceUrl"))
                .InstancePerLifetimeScope();
        }
    }
}
